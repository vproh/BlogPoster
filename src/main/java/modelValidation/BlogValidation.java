package modelValidation;

import models.Post;
import models.Role;
import models.Tag;
import models.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BlogValidation {
    private Validator validator;
    private List<String> errorMessages;

    public BlogValidation() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        errorMessages = new ArrayList<>();
    }

    public List<String> validate(User user)
    {
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        for (ConstraintViolation<User> violation : violations) {
            errorMessages.add(violation.getMessage());
        }
        return errorMessages;
    }

    public List<String> validate(Post post){
        Set<ConstraintViolation<Post>> violations = validator.validate(post);
        for (ConstraintViolation<Post> violation : violations) {
            errorMessages.add(violation.getMessage());
        }
        return errorMessages;
    }

    public List<String> validate(Tag tag) {
        Set<ConstraintViolation<Tag>> violations = validator.validate(tag);
        for(ConstraintViolation<Tag> violation : violations) {
            errorMessages.add(violation.getMessage());
        }

        return errorMessages;
    }

    public List<String> validate(Role role) {
        Set<ConstraintViolation<Role>> violations = validator.validate(role);
        for(ConstraintViolation<Role> violation : violations) {
            errorMessages.add(violation.getMessage());
        }

        return errorMessages;
    }

}
