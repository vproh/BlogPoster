package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static servlet.admin.TagDeleteServlet.DELETE_TAG_SERVLET_ADMIN;
import static servlet.admin.TagDeleteServlet.TAG_ID_PARAM;

@WebFilter(urlPatterns = {'/'+DELETE_TAG_SERVLET_ADMIN})
public class TagDeleteFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String tagId = httpServletRequest.getParameter(TAG_ID_PARAM);

        if (tagId == null) {
            httpResponse.sendRedirect("pageNotFound");
        } else {
            chain.doFilter(request, response);
        }
    }
    @Override
    public void destroy() {

    }
}