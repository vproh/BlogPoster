package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static servlet.AddPostServlet.POST_TITLE_PARAM;
import static servlet.SearchServlet.SEARCH_ID_PARAM;
import static servlet.SearchServlet.SEARCH_SERVLET;
import static servlet.SearchServlet.SEARCH_TYPE_PARAM;

@WebFilter(urlPatterns = {'/'+SEARCH_SERVLET})
public class SearchFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @SuppressWarnings("Duplicates")
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String searchType = httpServletRequest.getParameter(SEARCH_TYPE_PARAM);
        String searchId = httpServletRequest.getParameter(SEARCH_ID_PARAM);
        String postTitle = httpServletRequest.getParameter(POST_TITLE_PARAM);

        if (postTitle != null)
            chain.doFilter(request, response);
        else if (searchType == null || searchId == null) {
            httpResponse.sendRedirect("pageNotFound");
        } else {
            chain.doFilter(request, response);
        }
    }
    @Override
    public void destroy() {

    }
}