package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static servlet.ChangeUserServlet.CHANGE_USER_SERVLET;
import static servlet.ProfileServlet.USER_CHANGE_PARAM;

@WebFilter(urlPatterns = {'/'+CHANGE_USER_SERVLET})
public class ProfileFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String userChange = httpServletRequest.getParameter(USER_CHANGE_PARAM);

        if (userChange == null) {
            httpResponse.sendRedirect("pageNotFound");
        } else {
            chain.doFilter(request, response);
        }
    }
    @Override
    public void destroy() {

    }
}