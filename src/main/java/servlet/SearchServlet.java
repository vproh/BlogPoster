package servlet;

import models.Post;
import models.User;
import org.apache.log4j.Logger;
import services.PostPagination;
import services.PostService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static servlet.AddPostServlet.POST_TITLE_PARAM;
import static servlet.ExploreServlet.*;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.UserPostServlet.USER_POSTS_TYPE_PUBLISHED_SERVLET;

@WebServlet(urlPatterns = {"/search"})
public class SearchServlet extends HttpServlet {
    public static final String SEARCH_SERVLET = "search";
    public static final String SEARCH_TYPE_PARAM = "searchType";
    public static final String SEARCH_ID_PARAM = "id";
    private static final String SEARCH_USER_TYPE = "user";
    private static final String SEARCH_TAG_TYPE = "tag";
    private static final Logger LOG = Logger.getLogger(SearchServlet.class);


    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int currentPage = PostPagination.getCurrentPage(request.getParameter(CURRENT_PAGE_ATTR));
        int postAmount = 0;
        int id;
        PostService postService = new PostService();
        String searchType = request.getParameter(SEARCH_TYPE_PARAM);
        List<Post> postList = null;
        String postPart = request.getParameter(POST_TITLE_PARAM);
        User currentUser = (User) request.getSession().getAttribute(USER_SESSION_ATTR);

        try {
            if (searchType != null) {
                switch (searchType) {
                    case SEARCH_USER_TYPE:
                        id = Integer.parseInt(request.getParameter(SEARCH_ID_PARAM));
                        if (currentUser != null && id == currentUser.getUserId()) {
                            response.sendRedirect(USER_POSTS_TYPE_PUBLISHED_SERVLET);
                            return;
                        }
                        postAmount = postService.getVisiblePostsAmount(id);
                        postList = postService.getVisiblePostsByUserId(id, currentPage, POST_PER_PAGE);
                        break;
                    case SEARCH_TAG_TYPE:
                        id = Integer.parseInt(request.getParameter(SEARCH_ID_PARAM));
                        postAmount = postService.getPublishedPostsByTagIdAmount(id);
                        postList = postService.getPublishedPostsByTagId(id, currentPage, POST_PER_PAGE);
                        break;
                    default:
                        if (postPart == null)
                            return;
                        postAmount = postService.getSearchedPostsAmount(postPart);
                        postList = postService.getSearchedPosts(postPart, currentPage, POST_PER_PAGE);
                        break;
                }
            } else {
                if (postPart == null)
                    return;
                postAmount = postService.getSearchedPostsAmount(postPart);
                postList = postService.getSearchedPosts(postPart, currentPage, POST_PER_PAGE);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        request.setAttribute(POSTS_TO_SHOW_ATTR, postList); // TODO IF THERE IS NO POST IN JSP
        request.setAttribute(PAGES_NUMBER_ATTR, PostPagination.getPagesAmount(postAmount, POST_PER_PAGE));
        request.setAttribute(CURRENT_PAGE_ATTR, currentPage);

        if (request.getSession().getAttribute(USER_SESSION_ATTR) == null) {
            request.getRequestDispatcher(ExploreServlet.EXPLORE_JSP).forward(request, response);
        } else {
            request.setAttribute(CURRENT_USER_ATTR, request.getSession().getAttribute(USER_SESSION_ATTR));
            request.getRequestDispatcher(ExploreServlet.EXPLORE_JSP_AUTH).forward(request, response);
        }
    }
}
