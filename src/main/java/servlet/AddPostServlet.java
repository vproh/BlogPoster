package servlet;

import models.Tag;
import models.User;
import org.apache.log4j.Logger;
import services.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;

import static servlet.ExploreServlet.CURRENT_USER_ATTR;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.UserPostServlet.USER_POSTS_TYPE_PUBLISHED_SERVLET;

@WebServlet(urlPatterns = {"/create"})
@MultipartConfig
public class AddPostServlet extends HttpServlet {
    public static final String CREATE_POST_SERVLET = "create";
    public static final String POST_TITLE_PARAM = "postTitle";
    static final String POST_CONTENT_PARAM = "postContent";
    static final String POST_DESCRIPTION_PARAM = "postDescription";
    static final String POST_IMAGE_PARAM = "postImage";
    static final String POST_COLLABORATORS_PARAM = "collaborators";
    static final String POST_TAG_PARAM = "postTags";
    static final String AVAILABLE_COLLABORATION = "availableCollaborators";
    private static final String IS_PUBLISH_PARAM = "isPublish";
    private static final String PUBLISHED_MESSAGE = "Your post has been published successfully!";
    private static final String DRAFT_MESSAGE = "Your post has been saved successfully!";
    private static final String CREATE_ERROR = "errors";

    static final String INFO_MESSAGE_ATTR = "infoMessage";
    private static final String ADD_POST_JSP = "jsp/createPost.jsp";
    static final int MAX_COLLABORATOR = 5;

    private static final Logger LOG = Logger.getLogger(AddPostServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setAttribute(POST_TAG_PARAM, new TagService().getTagSet());
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        request.setAttribute(CURRENT_USER_ATTR, request.getSession().getAttribute(USER_SESSION_ATTR));
        request.getRequestDispatcher(ADD_POST_JSP).forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter(POST_TITLE_PARAM);
        String description = request.getParameter(POST_DESCRIPTION_PARAM);
        String content = request.getParameter(POST_CONTENT_PARAM);
        Part imgPart = request.getPart(POST_IMAGE_PARAM);
        List<User> users;
        Set<Tag> tagSet;
        String imageSrc = null;
        String message;
        boolean isPublishing = Boolean.parseBoolean(request.getParameter(IS_PUBLISH_PARAM));

        if (imgPart != null) {
            String fileName = UUID.randomUUID().toString() + '-' + Paths.get(imgPart.getSubmittedFileName()).getFileName().toString();
            UploadImage.uploadImage(fileName, imgPart.getInputStream(), ReadFromProperty.getProjectUploadsDirPath());
            imageSrc = UploadImage.uploadImage(fileName, imgPart.getInputStream(), request.getServletContext().getRealPath("") + ReadFromProperty.getUploadsDirName());
        }

        try {
            users = new UserService().getUsersByEmail((User) request.getSession().getAttribute(USER_SESSION_ATTR), request.getParameterValues(POST_COLLABORATORS_PARAM));
            tagSet = new TagService().getTagSetByNames (request.getParameter(POST_TAG_PARAM));
            List<String> errors = new PostService().addPost(title, description, content, imageSrc, tagSet, users, isPublishing);
            if (errors == null) {
                if (isPublishing)
                    message = PUBLISHED_MESSAGE;
                else
                    message = DRAFT_MESSAGE;

                request.getSession().setAttribute(INFO_MESSAGE_ATTR, message);
                response.sendRedirect(USER_POSTS_TYPE_PUBLISHED_SERVLET);
            } else {
                request.setAttribute(POST_TITLE_PARAM, title);
                request.setAttribute(POST_DESCRIPTION_PARAM, description);
                request.setAttribute(POST_CONTENT_PARAM, content);
                request.setAttribute(POST_IMAGE_PARAM, imageSrc);
                request.setAttribute(POST_COLLABORATORS_PARAM, users);
                request.setAttribute(CREATE_ERROR, errors);
                request.setAttribute(AVAILABLE_COLLABORATION, new ArrayList<>(Arrays.asList(new Integer[MAX_COLLABORATOR - users.size()])));

                doGet(request, response);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}