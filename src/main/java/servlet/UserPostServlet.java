package servlet;

import models.Post;
import models.User;
import org.apache.log4j.Logger;
import services.PostPagination;
import services.PostService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static servlet.AddPostServlet.INFO_MESSAGE_ATTR;
import static servlet.ExploreServlet.*;

@WebServlet(urlPatterns = {"/myPosts"})
public class UserPostServlet extends HttpServlet {
    private static final String TAB_1_ATTR = "tab1Active";
    private static final String TAB_2_ATTR = "tab2Active";
    private static final String TAB_3_ATTR = "tab3Active";
    private static final Logger LOG = Logger.getLogger(UserPostServlet.class);

    private static final String USER_POST_TO_SHOW_ATTR = "posts";
    private static final String USER_POSTS_JSP = "jsp/myPosts.jsp";
    static final String USER_POSTS_TYPE_PUBLISHED_SERVLET = "/myPosts?postType=published";
    public static final String USER_POSTS_SERVLET = "myPosts";
    public static final String POST_TYPE_PARAM = "postType";
    static final String DRAFT_POST_TYPE = "drafts";
    static final String COLLABORATION_POST_TYPE = "collaboration";
    private static final String PUBLISHED_POST_AMOUNT = "publishedAmount";
    private static final String DRAFT_POST_AMOUNT = "draftsAmount";
    private static final String COLLABORATION_POST_AMOUNT = "collabAmount";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int currentPage = PostPagination.getCurrentPage(request.getParameter("currentPage"));
        int postAmount;
        int userId;
        PostService postService = new PostService();
        User currentUser = (User) request.getSession().getAttribute(LoginServlet.USER_SESSION_ATTR);
        String postType = request.getParameter(POST_TYPE_PARAM);

        userId = currentUser.getUserId();
        request.setAttribute(CURRENT_USER_ATTR, currentUser);
        try {
            request.setAttribute(PUBLISHED_POST_AMOUNT, postService.getPublishedOwnerPostsAmount(userId));
            request.setAttribute(DRAFT_POST_AMOUNT, postService.getOwnerDraftAmount(userId));
            request.setAttribute(COLLABORATION_POST_AMOUNT, postService.getCollaboratingPostsAmount(userId));
            request.setAttribute(POST_TYPE_PARAM, postType);

            switch (postType) {
                case DRAFT_POST_TYPE:
                    request.setAttribute(TAB_2_ATTR, "active");
                    request.setAttribute(POST_TYPE_PARAM, DRAFT_POST_TYPE);
                    postAmount = postService.getOwnerDraftAmount(userId);
                    forwardToMyPosts(request, response, postService.getOwnerDrafts(userId, currentPage, POST_PER_PAGE), postAmount, currentPage);
                    break;
                case COLLABORATION_POST_TYPE:
                    request.setAttribute(TAB_3_ATTR, "active");
                    request.setAttribute(POST_TYPE_PARAM, COLLABORATION_POST_TYPE);
                    postAmount = postService.getCollaboratingPostsAmount(userId);
                    forwardToMyPosts(request, response, postService.getCollaboratingPosts(userId, currentPage, POST_PER_PAGE), postAmount, currentPage);
                    break;
                default:
                    request.setAttribute(TAB_1_ATTR, "active");
                    postAmount = postService.getPublishedOwnerPostsAmount(userId);
                    forwardToMyPosts(request, response, postService.getPublishedOwnerPosts(userId, currentPage, POST_PER_PAGE), postAmount, currentPage);
                    break;
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    private void forwardToMyPosts(HttpServletRequest request, HttpServletResponse response, List<Post> posts, int postAmount, int curPage) throws ServletException, IOException {
        request.setAttribute(USER_POST_TO_SHOW_ATTR, posts);
        request.setAttribute(CURRENT_PAGE_ATTR, curPage);
        request.setAttribute(PAGES_NUMBER_ATTR, PostPagination.getPagesAmount(postAmount, POST_PER_PAGE));
        request.setAttribute(INFO_MESSAGE_ATTR, request.getSession().getAttribute(INFO_MESSAGE_ATTR));
        request.getSession().removeAttribute(INFO_MESSAGE_ATTR);

        request.getRequestDispatcher(USER_POSTS_JSP).forward(request, response);
    }
}
