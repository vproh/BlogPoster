package servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/pageNotFound"})
public class PageNotFoundServlet extends HttpServlet {
    private static final String PAGE_NOT_FOUND_PATH = "html/pageNotFoundError.html";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(PAGE_NOT_FOUND_PATH);
    }
}
