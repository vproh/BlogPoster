package servlet;

import models.Post;
import org.apache.log4j.Logger;
import services.PostPagination;
import services.PostService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static servlet.LoginServlet.LOGIN_ERROR_KEY;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.RegisterServlet.REGISTER_ERRORS;

@WebServlet(urlPatterns = {"/explore"})
public class ExploreServlet extends HttpServlet {
    public static final String EXPLORE_SERVLET = "explore";
    static final String EXPLORE_JSP = "jsp/explore.jsp";
    static final String EXPLORE_JSP_AUTH = "jsp/exploreAuth.jsp";

    static final String POSTS_TO_SHOW_ATTR = "explorePost";
    static final String PAGES_NUMBER_ATTR = "numOfPages";
    static final String CURRENT_PAGE_ATTR = "currentPage";
    public static final String CURRENT_USER_ATTR = "currUser";

    static final int POST_PER_PAGE = 2;
    private static final Logger LOG = Logger.getLogger(ExploreServlet.class);


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getAttribute(LOGIN_ERROR_KEY) == null && request.getAttribute(REGISTER_ERRORS) == null) {
            request.setAttribute(CURRENT_USER_ATTR, request.getSession().getAttribute(USER_SESSION_ATTR));
            forwardToJsp(request, response, EXPLORE_JSP_AUTH);
        } else {
            forwardToJsp(request, response, EXPLORE_JSP);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute(USER_SESSION_ATTR) == null) {
            forwardToJsp(request, response, EXPLORE_JSP);
        } else {
            request.setAttribute(CURRENT_USER_ATTR, request.getSession().getAttribute(USER_SESSION_ATTR));
            forwardToJsp(request, response, EXPLORE_JSP_AUTH);
        }
    }

    private void forwardToJsp(HttpServletRequest request, HttpServletResponse response, String pathToForward) throws ServletException, IOException {
        int currentPage = PostPagination.getCurrentPage(request.getParameter(CURRENT_PAGE_ATTR));
        int postAmount;
        PostService postService = new PostService();
        List<Post> posts;

        try {
            posts = postService.getPublishedPosts(currentPage, POST_PER_PAGE);
            postAmount = postService.getPublishedPostAmount();

            request.setAttribute(POSTS_TO_SHOW_ATTR, posts);
            request.setAttribute(PAGES_NUMBER_ATTR, PostPagination.getPagesAmount(postAmount, POST_PER_PAGE));
            request.setAttribute(CURRENT_PAGE_ATTR, currentPage);

            request.getRequestDispatcher(pathToForward).forward(request, response);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
