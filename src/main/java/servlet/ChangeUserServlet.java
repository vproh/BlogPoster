package servlet;

import models.User;
import org.apache.log4j.Logger;
import services.UserService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static servlet.AddPostServlet.INFO_MESSAGE_ATTR;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.LogoutServlet.LOGOUT_SERVLET;
import static servlet.ProfileServlet.PROFILE_SERVLET;
import static servlet.ProfileServlet.UPDATE_USER_ERROR_ATTR;
import static servlet.ProfileServlet.USER_CHANGE_PARAM;

@WebServlet(urlPatterns = "/changeUser")
public class ChangeUserServlet extends HttpServlet {
    public static final String CHANGE_USER_SERVLET = "changeUser";
    private static final Logger LOG = Logger.getLogger(ProfileServlet.class);
    private static final String USER_CHANGE_PASSWORD = "security";
    private static final String USER_DELETE_ACCOUNT = "deleteAcc";
    private static final String USER_NEW_PASSWORD_PARAM = "userNewPass";
    private static final String USER_NEW_PASSWORD_CONFIRM_PARAM = "confirmNewPass";
    private static final String USER_CURRENT_CHANGE_PASSWORD_PARAM = "currentUserPassword";
    private static final String USER_CURRENT_DELETE_PASSWORD_PARAM = "currentPass";
    private static final String USER_CURRENT_DELETE_CONFIRM_PASSWORD_PARAM = "currentPassConfirm";
    private static final String USER_NAME_PARAM = "userName";
    private static final String USER_LAST_NAME_PARAM = "userLastName";
    private static final String USER_EMAIL_PARAM = "userEmail";
    private static final String GENERAL_SETTINGS_SUCCESS_MESSAGE = "General settings has been updated successfully!";
    private static final String PASSWORD_CHANGE_SUCCESS_MESSAGE = "Password has been changed successfully!";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String change = request.getParameter(USER_CHANGE_PARAM);

        UserService userService = new UserService();
        User currentUser = (User) request.getSession().getAttribute(USER_SESSION_ATTR);
        List<String> errors = null;
        String message = null;

        try {
            switch (change) {
                case USER_CHANGE_PASSWORD:
                    String newPass = request.getParameter(USER_NEW_PASSWORD_PARAM);
                    String confirmNew = request.getParameter(USER_NEW_PASSWORD_CONFIRM_PARAM);
                    String currentPass = request.getParameter(USER_CURRENT_CHANGE_PASSWORD_PARAM);
                    errors = userService.updateSecurityInfo(currentUser, newPass, confirmNew, currentPass);
                    if (errors == null) {
                        message = PASSWORD_CHANGE_SUCCESS_MESSAGE;
                    }
                    break;
                case USER_DELETE_ACCOUNT:
                    String currentPassword = request.getParameter(USER_CURRENT_DELETE_PASSWORD_PARAM);
                    String confirmCurrentPassword = request.getParameter(USER_CURRENT_DELETE_CONFIRM_PASSWORD_PARAM);
                    errors = userService.deleteUserAccount(currentUser, currentPassword, confirmCurrentPassword);
                    if (errors == null) {
                        response.sendRedirect(LOGOUT_SERVLET);
                        return;
                    }
                    break;
                default:
                    String name = request.getParameter(USER_NAME_PARAM);
                    String lastName = request.getParameter(USER_LAST_NAME_PARAM);
                    String email = request.getParameter(USER_EMAIL_PARAM);

                    errors = userService.updateGeneralInfo(currentUser, name, lastName, email);
                    if (errors == null) {
                        message = GENERAL_SETTINGS_SUCCESS_MESSAGE;
                    }
                    break;
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        if (errors != null)
            request.getSession().setAttribute(UPDATE_USER_ERROR_ATTR, errors);
        request.getSession().setAttribute(INFO_MESSAGE_ATTR, message);

        response.sendRedirect(PROFILE_SERVLET);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
