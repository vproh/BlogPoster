package servlet;

import models.Post;
import models.Tag;
import models.User;
import org.apache.log4j.Logger;
import services.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;

import static servlet.AddPostServlet.*;
import static servlet.LoginServlet.USER_SESSION_ATTR;
import static servlet.SinglePostServlet.POST_ID_PARAM;
import static servlet.UserPostServlet.*;

@WebServlet(urlPatterns = {"/editPost"})
@MultipartConfig
public class EditPostServlet extends HttpServlet {
    private static final String SUCCESSFULLY_UPDATE_MESSAGE = "Post has been updated successfully!";
    private static final Logger LOG = Logger.getLogger(EditPostServlet.class);
    private static final String POST_ATTR = "post";
    private static final String EDIT_POST_JSP = "jsp/editPost.jsp";
    private static final String EDIT_POST_ERROR = "editPostError";
    public static final String EDIT_POST_SERVLET = "editPost";


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter(POST_TITLE_PARAM);
        String description = request.getParameter(POST_DESCRIPTION_PARAM);
        String content = request.getParameter(POST_CONTENT_PARAM);
        String postType = request.getParameter(POST_TYPE_PARAM);
        Part imgPart = request.getPart(POST_IMAGE_PARAM);
        List<User> users;
        Set<Tag> tagSet;
        String imageSrc = null;
        PostService postService = new PostService();
        int postId = Integer.parseInt(request.getParameter(POST_ID_PARAM));
        List<String> errors;

        if (imgPart != null) {
            String fileName = UUID.randomUUID().toString() + '-' + Paths.get(imgPart.getSubmittedFileName()).getFileName().toString();
            UploadImage.uploadImage(fileName, imgPart.getInputStream(), ReadFromProperty.getProjectUploadsDirPath());
            imageSrc = UploadImage.uploadImage(fileName, imgPart.getInputStream(), request.getServletContext().getRealPath("") + ReadFromProperty.getUploadsDirName());
        }
        try {
            users = new UserService().getUsersByEmail((User) request.getSession().getAttribute(USER_SESSION_ATTR), request.getParameterValues(POST_COLLABORATORS_PARAM));
            tagSet = new TagService().getTagSetByNames (request.getParameter(POST_TAG_PARAM));

            switch (postType) {
                case COLLABORATION_POST_TYPE:
                    errors = postService.updateCollabPost(postId, title, description, content, imageSrc, tagSet);
                    break;
                    case DRAFT_POST_TYPE:
                        errors = postService.updatePost(postId, title, description, content, imageSrc, users, tagSet);
                        break;
                        default:
                            errors = postService.updatePost(postId, title, description, content, imageSrc, users, tagSet);
                            break;
                }

            if (errors == null) {
                request.getSession().setAttribute(INFO_MESSAGE_ATTR, SUCCESSFULLY_UPDATE_MESSAGE);
                response.sendRedirect(USER_POSTS_SERVLET + "?" + POST_TYPE_PARAM + "=" + postType);
            } else {
                request.setAttribute(EDIT_POST_ERROR, errors);
                doGet(request, response);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int postId = Integer.parseInt(request.getParameter(POST_ID_PARAM));

        try {
            Post post = new PostService().getPostById(postId);
            String postType = request.getParameter(POST_TYPE_PARAM);
            request.setAttribute(POST_TAG_PARAM, new TagService().getTagSet());
            request.setAttribute(POST_TYPE_PARAM, postType);
            post.getUsers().remove(0);
            request.setAttribute(POST_ATTR, post);
            request.setAttribute(AVAILABLE_COLLABORATION, new ArrayList<>(Arrays.asList(new Integer[MAX_COLLABORATOR - post.getUsers().size()])));
            request.getRequestDispatcher(EDIT_POST_JSP).forward(request, response);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
