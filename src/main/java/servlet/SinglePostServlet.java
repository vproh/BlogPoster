package servlet;

import models.Post;
import models.User;
import org.apache.log4j.Logger;
import services.PostService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static servlet.ExploreServlet.CURRENT_USER_ATTR;
import static servlet.LoginServlet.USER_SESSION_ATTR;

@WebServlet(urlPatterns = {"/post"})
public class SinglePostServlet extends HttpServlet {
    public static final String POST_ID_PARAM = "postId";
    private static final String POST_ATTR = "post";
    private static final String USER_SINGLE_POST_JSP = "jsp/singlePost.jsp";
    private static final String ADMIN_SINGLE_POST_JSP = "jsp/admin/singlePost.jsp";

    private static final Logger LOG = Logger.getLogger(SinglePostServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int postId = Integer.parseInt(request.getParameter(POST_ID_PARAM));
        User currentUser = (User) request.getSession().getAttribute(USER_SESSION_ATTR);
        UserService userService = new UserService();

        try {
            Post post = new PostService().getPostById(postId);
            request.setAttribute(CURRENT_USER_ATTR, currentUser);
            request.setAttribute(POST_ATTR, post);
            if (currentUser == null || !userService.isUserAdmin(currentUser.getEmail()))
                request.getRequestDispatcher(USER_SINGLE_POST_JSP).forward(request, response);
            else if (userService.isUserAdmin(currentUser.getEmail()))
                request.getRequestDispatcher(ADMIN_SINGLE_POST_JSP).forward(request, response);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
