package models;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class Role {
    private static final String DEFAULT_USER_ROLE = "user";
    private static final String ADMIN_ROLE = "admin";

    @Min(1)
    @Positive
    private int roleId;

    @NotNull private String name;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role() {
        roleId = 1;
        name = DEFAULT_USER_ROLE;
    }

    public boolean isRoleUser() {
        return DEFAULT_USER_ROLE.equals(name);
    }

    public boolean isRoleAdmin() {
        return ADMIN_ROLE.equals(name);
    }
}
