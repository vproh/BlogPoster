package models;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

public class  Post {
    @Positive private int postId;
    @NotNull private String title;
    @NotNull private String description;
    @NotNull private String imageUrl;
    @NotNull private String content;
    @PastOrPresent private LocalDate dateOfCreate;
    private LocalDate dateOfUpdate;
    private LocalDate dateOfPublishing;
    private LocalTime timeOfUpdate;
    @NotNull private Set<Tag> tagList;
    @NotNull private List<User> users;

    public Post() {
        postId = 1;
    }

    public Post(@NotNull String title, @NotNull String description, @NotNull String imageUrl, @NotNull String content, @PastOrPresent LocalDate dateOfCreate, LocalDate dateOfUpdate, LocalDate dateOfPublishing, LocalTime timeOfUpdate, @NotNull Set<Tag> tagList, @NotNull List<User> users) {
        postId = 1;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.content = content;
        this.dateOfCreate = dateOfCreate;
        this.dateOfUpdate = dateOfUpdate;
        this.dateOfPublishing = dateOfPublishing;
        this.timeOfUpdate = timeOfUpdate;
        this.tagList = tagList;
        this.users = users;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int id) {
        this.postId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDate getDateOfCreate() {
        return dateOfCreate;
    }

    public void setDateOfCreate(LocalDate dateOfCreate) {
        this.dateOfCreate = dateOfCreate;
    }

    public LocalDate getDateOfUpdate() {
        return dateOfUpdate;
    }

    public void setDateOfUpdate(LocalDate dateOfUpdate) {
        this.dateOfUpdate = dateOfUpdate;
    }

    public LocalDate getDateOfPublishing() {
        return dateOfPublishing;
    }

    public void setDateOfPublishing(LocalDate dateOfPublishing) {
        this.dateOfPublishing = dateOfPublishing;
    }

    public LocalTime getTimeOfUpdate() {
        return timeOfUpdate;
    }

    public void setTimeOfUpdate(LocalTime timeOfUpdate) {
        this.timeOfUpdate = timeOfUpdate;
    }

    public Set<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(Set<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}

