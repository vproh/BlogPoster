package models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class Tag {
    @Positive private int tagId;
    @NotNull private String name;

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
