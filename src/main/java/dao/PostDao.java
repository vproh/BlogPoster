package dao;

import dao.interfaces.IDaoPost;
import dataBase.DataBaseConnection;
import models.Post;
import models.Tag;
import models.User;

import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;


public class PostDao implements IDaoPost {
    /* Post table column names */
    private static final String ID_COLUMN_NAME = "post_id";
    private static final String TITLE_COLUMN_NAME = "title";
    private static final String DESCRIPTION_COLUMN_NAME = "description";
    private static final String CONTENT_COLUMN_NAME = "content";
    private static final String IMAGE_URL_COLUMN_NAME = "image_url";
    private static final String CREATE_DATE_COLUMN_NAME = "date_of_create";
    private static final String UPDATE_DATE_COLUMN_NAME = "date_of_update";
    private static final String PUBLISH_DATE_COLUMN_NAME = "date_of_publish";
    private static final String UPDATE_TIME_COLUMN_NAME = "time_of_update";
    private static final String TAG_IDS_COLUMN_NAME = "tag_id";
    private static final String USER_ID_COLUMN_NAME = "users_id";

    /* SQL requests*/
    private static final String GET_POST_BY_ID_REQUEST = "SELECT * FROM post WHERE post_id=?";
    private static final String ADD_POST_REQUEST = "INSERT INTO post(" +
            TITLE_COLUMN_NAME + ", " + DESCRIPTION_COLUMN_NAME + ", " +
            CONTENT_COLUMN_NAME + ", " + IMAGE_URL_COLUMN_NAME + ", " +
            CREATE_DATE_COLUMN_NAME + ", " + PUBLISH_DATE_COLUMN_NAME + ", " +
            UPDATE_DATE_COLUMN_NAME + ", " + UPDATE_TIME_COLUMN_NAME + ", " +
            TAG_IDS_COLUMN_NAME + ", " + USER_ID_COLUMN_NAME +
            ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String DROP_POST_BY_ID_REQUEST = "DELETE FROM post WHERE post_id=?";
    private static final String UPDATE_POST_BY_ID_REQUEST = "UPDATE post SET " +
            TITLE_COLUMN_NAME + " =?, " +
            DESCRIPTION_COLUMN_NAME + "=?, " + CONTENT_COLUMN_NAME + "=?, " +
            IMAGE_URL_COLUMN_NAME + "=?, " + CREATE_DATE_COLUMN_NAME + "=?, " +
            PUBLISH_DATE_COLUMN_NAME + "=?, " + UPDATE_DATE_COLUMN_NAME + "=?, " +
            UPDATE_TIME_COLUMN_NAME + "=?, " + TAG_IDS_COLUMN_NAME + "=?, " +
            USER_ID_COLUMN_NAME + "=? " + "WHERE post_id=?";

    private static final String GET_ALL_POSTS_REQUEST = "SELECT * from post";

    private static final String GET_PUBLISHED_POSTS = "SELECT * FROM post WHERE " + PUBLISH_DATE_COLUMN_NAME + " NOTNULL";
    private static final String GET_LIMIT_PUBLISHED_POSTS = GET_PUBLISHED_POSTS + " LIMIT ? OFFSET ?";

    private static final String GET_SEARCHED_POSTS = GET_PUBLISHED_POSTS + " AND LOWER(" + TITLE_COLUMN_NAME + ") LIKE ?";
    private static final String GET_LIMIT_SEARCHED_POSTS = GET_SEARCHED_POSTS + " LIMIT ? OFFSET ?";

    private static final String GET_VISIBLE_POSTS_BY_USER_ID = GET_PUBLISHED_POSTS + " AND ? = ANY (" + USER_ID_COLUMN_NAME + ")";
    private static final String GET_VISIBLE_LIMIT_POSTS_BY_USER_ID = GET_VISIBLE_POSTS_BY_USER_ID + " LIMIT ? OFFSET ?";

    private static final String GET_PUBLISHED_POSTS_BY_ID = GET_PUBLISHED_POSTS + " AND ? = " + USER_ID_COLUMN_NAME + "[?]";
    private static final String GET_PUBLISHED_LIMIT_POSTS_BY_ID = GET_PUBLISHED_POSTS_BY_ID + " LIMIT ? OFFSET ?";

    private static final String GET_UNPUBLISHED_POSTS_BY_ID = "SELECT * FROM post WHERE " + PUBLISH_DATE_COLUMN_NAME + " IS NULL AND " +
            "? = " + USER_ID_COLUMN_NAME + "[?]";
    private static final String GET_UNPUBLISHED_LIMIT_POSTS_BY_ID = GET_UNPUBLISHED_POSTS_BY_ID + " LIMIT ? OFFSET ?";

    private static final String GET_COLLAB_POST_BY_ID = "SELECT * FROM post WHERE ? != " + USER_ID_COLUMN_NAME + "[?] AND ? = ANY (" +
            USER_ID_COLUMN_NAME + ")";
    private static final String GET_COLLAB_LIMIT_POST_BY_ID = GET_COLLAB_POST_BY_ID + " LIMIT ? OFFSET ?";

    private static final String GET_PUBLISHED_POSTS_BY_TAG = GET_PUBLISHED_POSTS + " AND ? = ANY (" + TAG_IDS_COLUMN_NAME + ")";
    private static final String GET_PUBLISHED_LIMIT_POSTS_BY_TAG = GET_PUBLISHED_POSTS_BY_TAG + " LIMIT ? OFFSET ?";

    private static final String UPDATE_PUBLISH_DATE_BY_ID = "UPDATE post SET " +
            PUBLISH_DATE_COLUMN_NAME + "=?, " + UPDATE_DATE_COLUMN_NAME + "=?, " +
            UPDATE_TIME_COLUMN_NAME + "=? WHERE post_id=?";

    @Override
    public Post getPostById(int id) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_POST_BY_ID_REQUEST);

        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();

        Post post = getPost(resultSet);

        resultSet.close();
        preparedStatement.close();
        connection.close();

        return post;
    }

    @Override
    public void addPost(Post post) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(ADD_POST_REQUEST);

        LocalDate publicationDate = post.getDateOfPublishing();
        LocalDate updateDate = post.getDateOfUpdate();
        LocalTime updateTime = post.getTimeOfUpdate();

        Date dPublication = null;
        Date dUpdate = null;
        Time tUpdate = null;

        if (publicationDate != null)
            dPublication = Date.valueOf(publicationDate);
        if (updateDate != null)
            dUpdate = Date.valueOf(updateDate);
        if (updateTime != null)
            tUpdate = Time.valueOf(updateTime);

        preparedStatement.setString(1, post.getTitle());
        preparedStatement.setString(2, post.getDescription());
        preparedStatement.setString(3, post.getContent());
        preparedStatement.setString(4, post.getImageUrl());
        preparedStatement.setDate(5, Date.valueOf(post.getDateOfCreate()));
        preparedStatement.setDate(6, dPublication);
        preparedStatement.setDate(7, dUpdate);
        preparedStatement.setTime(8, tUpdate);
        preparedStatement.setArray(9, connection.createArrayOf("int4", post.getTagList().stream().map(Tag::getTagId).toArray()));
        preparedStatement.setArray(10, connection.createArrayOf("int4", post.getUsers().stream().map(User::getUserId).toArray()));

        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }

    @Override
    public void deletePostById(int id) throws SQLException {
        DeleteRow.deleteRowByIdAndRequest(id, DROP_POST_BY_ID_REQUEST);
    }

    @Override
    public void updatePostById(Post post) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_POST_BY_ID_REQUEST);

        LocalDate dateOfCreate = post.getDateOfCreate();
        LocalDate dateOfPublishing = post.getDateOfPublishing();
        LocalDate dateOfUpdate = post.getDateOfUpdate();
        LocalTime timeOfUpdate = post.getTimeOfUpdate();


        Date createDate = null;
        Date publicationDate = null;
        Date updateDate = null;
        Time updateTime = null;

        if (dateOfCreate != null)
            createDate = Date.valueOf(dateOfCreate);
        if (dateOfPublishing != null)
            publicationDate = Date.valueOf(dateOfPublishing);
        if (dateOfUpdate != null)
            updateDate = Date.valueOf(dateOfUpdate);
        if (timeOfUpdate != null)
            updateTime = Time.valueOf(timeOfUpdate);

        preparedStatement.setString(1, post.getTitle());
        preparedStatement.setString(2, post.getDescription());
        preparedStatement.setString(3, post.getContent());
        preparedStatement.setString(4, post.getImageUrl());
        preparedStatement.setDate(5, createDate);
        preparedStatement.setDate(6, publicationDate);
        preparedStatement.setDate(7, updateDate);
        preparedStatement.setTime(8, updateTime);
        preparedStatement.setArray(9, connection.createArrayOf("int4", post.getTagList().stream().map(Tag::getTagId).toArray()));
        preparedStatement.setArray(10, connection.createArrayOf("int4", post.getUsers().stream().map(User::getUserId).toArray()));
        preparedStatement.setInt(11, post.getPostId());

        preparedStatement.executeUpdate();
    }

    @Override
    public List<Post> getAllPost() throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_POSTS_REQUEST);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Post> posts = new ArrayList<>();

        while(resultSet.next())
            posts.add(getPostById(resultSet.getInt(ID_COLUMN_NAME)));

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if (posts.isEmpty())
            return  null;

        return posts;
    }

    @Override
    public List<Post> getLimitExplorePost(int postAmount, int offset) throws SQLException {
        return getPosts(postAmount, offset, GET_LIMIT_PUBLISHED_POSTS);
    }

    @Override
    public List<Post> getAllPublishedPosts() throws SQLException {
        return getPosts(GET_PUBLISHED_POSTS);
    }

    @Override
    public List<Post> getPublishedPostsById(int userId, int ownerIndex) throws SQLException {
        return getPosts(userId, ownerIndex, GET_PUBLISHED_POSTS_BY_ID);
    }

    @Override
    public List<Post> getDraftPostsById(int userId, int ownerIndex) throws SQLException {
        return getPosts(userId, ownerIndex, GET_UNPUBLISHED_POSTS_BY_ID);
    }

    @Override
    public List<Post> getCollabPostsById(int userId, int ownerIndex) throws SQLException {
        return getPosts(userId, ownerIndex, userId, GET_COLLAB_POST_BY_ID);
    }

    @Override
    public List<Post> getVisiblePostsById(int userId) throws SQLException {
        return getPosts(userId, GET_VISIBLE_POSTS_BY_USER_ID);
    }

    /* Limit posts */
    @Override
    public List<Post> getVisiblePostsById(int userId, int limit, int offset) throws SQLException {
        return getPosts(userId, limit, offset, GET_VISIBLE_LIMIT_POSTS_BY_USER_ID);
    }

    @Override
    public List<Post> getPublishedPostsById(int userId, int ownerIndex, int limit, int offset) throws SQLException {
        return getPosts(userId, ownerIndex, limit, offset, GET_PUBLISHED_LIMIT_POSTS_BY_ID);
    }

    @Override
    public List<Post> getDraftPostsById(int userId, int ownerIndex, int limit, int offset) throws SQLException {
        return getPosts(userId, ownerIndex, limit, offset, GET_UNPUBLISHED_LIMIT_POSTS_BY_ID);
    }

    @Override
    public List<Post> getCollaborationPostsById(int userId, int ownerIndex, int limit, int offset) throws SQLException {
        return getPosts(userId, ownerIndex, userId, limit, offset, GET_COLLAB_LIMIT_POST_BY_ID);
    }

    @Override
    public List<Post> getPublishedPostsByTagId(int tagId, int limit, int offset) throws SQLException {
        return getPosts(tagId, limit, offset, GET_PUBLISHED_LIMIT_POSTS_BY_TAG);
    }

    @Override
    public List<Post> getPublishedPostsByTagId(int tagId) throws SQLException {
        return getPosts(tagId, GET_PUBLISHED_POSTS_BY_TAG);
    }

    @Override
    public void updatePostPublishDate(int postId, LocalDate publishDate, LocalDate updateDate, LocalTime updateTime) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PUBLISH_DATE_BY_ID);

        preparedStatement.setDate(1, Date.valueOf(publishDate));
        preparedStatement.setDate(2, Date.valueOf(updateDate));
        preparedStatement.setTime(3, Time.valueOf(updateTime));
        preparedStatement.setInt(4, postId);

        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }

    @Override
    public List<Post> getSearchedPosts(String postTitlePart) throws SQLException {
        return getPosts("%" + postTitlePart + "%", GET_SEARCHED_POSTS);
    }

    @Override
    public List<Post> getSearchedPosts(String postTitlePart, int limit, int offset) throws SQLException {
        return getPosts("%" + postTitlePart + "%", limit, offset, GET_LIMIT_SEARCHED_POSTS);
    }

    /* private methods */

    private List<Post> getPosts(String param1, int param2, int param3, String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        preparedStatement.setString(1, param1);
        preparedStatement.setInt(2, param2);
        preparedStatement.setInt(3, param3);

        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if(posts.isEmpty())
            return null;
        return posts;
    }

    private List<Post> getPosts(String param, String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        preparedStatement.setString(1, param);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if(posts.isEmpty())
            return null;
        return posts;
    }

    private Post getPost(ResultSet resultSet) throws SQLException {
        Post post = null;
        Date date;
        Time time;

        if(resultSet.next()) {
            post = new Post();
            post.setPostId(resultSet.getInt(ID_COLUMN_NAME));
            post.setTitle(resultSet.getString(TITLE_COLUMN_NAME));
            post.setDescription(resultSet.getString(DESCRIPTION_COLUMN_NAME));
            post.setContent(resultSet.getString(CONTENT_COLUMN_NAME));
            post.setImageUrl(resultSet.getString(IMAGE_URL_COLUMN_NAME));

            date = resultSet.getDate(CREATE_DATE_COLUMN_NAME);
            if(date == null) post.setDateOfCreate(null);
            else post.setDateOfCreate(date.toLocalDate());

            date = resultSet.getDate(UPDATE_DATE_COLUMN_NAME);
            if(date == null) post.setDateOfUpdate(null);
            else post.setDateOfUpdate(date.toLocalDate());

            date = resultSet.getDate(PUBLISH_DATE_COLUMN_NAME);
            if(date == null) post.setDateOfPublishing(null);
            else post.setDateOfPublishing(date.toLocalDate());

            time = resultSet.getTime(UPDATE_TIME_COLUMN_NAME);
            if(time == null) post.setTimeOfUpdate(null);
            else post.setTimeOfUpdate(time.toLocalTime());

            post.setTagList(getPostTags((Integer[]) resultSet.getArray(TAG_IDS_COLUMN_NAME).getArray()));
            post.setUsers(getPostUsers((Integer[]) resultSet.getArray(USER_ID_COLUMN_NAME).getArray()));
        }

        return post;
    }

    private Set<Tag> getPostTags(Integer[] array) throws SQLException {
        TagDao tagDao = new TagDao();
        Set<Tag> tags = new HashSet<>();

        for (Integer anArray : array) tags.add(tagDao.getTagById(anArray));

        return tags;
    }

    private List<User> getPostUsers(Integer[] array) throws SQLException {
        UserDao userDao = new UserDao();
        List<User> users = new ArrayList<>();

        for (Integer i : array) users.add(userDao.getUserById(i));

        return users;
    }

    private List<Post> getPosts(String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();
        if(posts.isEmpty())
            return null;
        return posts;
    }

    private List<Post> getPosts(int param, String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        preparedStatement.setInt(1, param);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if(posts.isEmpty())
            return null;

        return posts;
    }

    private List<Post> getPosts(int param1, int param2, String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        preparedStatement.setInt(1, param1);
        preparedStatement.setInt(2, param2);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if(posts.isEmpty())
            return null;
        return posts;
    }

    private List<Post> getPosts(int param1, int param2, int param3, int param4, String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        preparedStatement.setInt(1, param1);
        preparedStatement.setInt(2, param2);
        preparedStatement.setInt(3, param3);
        preparedStatement.setInt(4, param4);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if(posts.isEmpty())
            return null;
        return posts;
    }

    private List<Post> getPosts(int param1, int param2, int param3, String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        preparedStatement.setInt(1, param1);
        preparedStatement.setInt(2, param2);
        preparedStatement.setInt(3, param3);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if(posts.isEmpty())
            return null;
        return posts;
    }

    private List<Post> getPosts(int param1, int param2, int param3, int param4, int param5, String request) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        preparedStatement.setInt(1, param1);
        preparedStatement.setInt(2, param2);
        preparedStatement.setInt(3, param3);
        preparedStatement.setInt(4, param4);
        preparedStatement.setInt(5, param5);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Post> posts = new ArrayList<>();

        Post post = getPost(resultSet);
        while (post != null) {
            posts.add(post);
            post = getPost(resultSet);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if(posts.isEmpty())
            return null;
        return posts;
    }
}
