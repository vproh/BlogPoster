package dao;

import dataBase.DataBaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddRow {
    public static void addOneElementRow(String addTagRequest, String name) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(addTagRequest);

        preparedStatement.setString(1, name);

        preparedStatement.executeUpdate();
        preparedStatement.close();
        connection.close();
    }
}
