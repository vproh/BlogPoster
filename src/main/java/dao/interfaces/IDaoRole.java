package dao.interfaces;

import models.Role;

import java.sql.SQLException;
import java.util.Set;

public interface IDaoRole {
    Role getRoleById(int id) throws SQLException;
    void addRole(Role role) throws SQLException;
    void deleteRoleById(int id) throws SQLException;
    void updateRoleById(Role role) throws SQLException;
    Set<Role> getAllRole() throws SQLException;
}
