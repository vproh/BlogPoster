package dao.interfaces;

import models.Post;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface IDaoPost {
    Post getPostById(int id) throws SQLException;
    void addPost(Post post) throws SQLException;
    void deletePostById(int id) throws SQLException;
    void updatePostById(Post post) throws SQLException;
    List<Post> getAllPost() throws SQLException;

    List<Post> getLimitExplorePost(int postAmount, int offset) throws SQLException;

    List<Post> getAllPublishedPosts() throws SQLException;

    List<Post> getPublishedPostsById(int userId, int ownerIndex) throws SQLException;

    List<Post> getDraftPostsById(int userId, int ownerIndex) throws SQLException;

    List<Post> getCollabPostsById(int userId, int ownerIndex) throws SQLException;

    List<Post> getVisiblePostsById(int userId) throws SQLException;

    /* Limit posts */
    List<Post> getVisiblePostsById(int userId, int limit, int offset) throws SQLException;

    List<Post> getPublishedPostsById(int userId, int ownerIndex, int limit, int offset) throws SQLException;

    List<Post> getDraftPostsById(int userId, int ownerIndex, int limit, int offset) throws SQLException;

    List<Post> getCollaborationPostsById(int userId, int ownerIndex, int limit, int offset) throws SQLException;

    List<Post> getPublishedPostsByTagId(int tagId, int limit, int offset) throws SQLException;

    List<Post> getPublishedPostsByTagId(int tagId) throws SQLException;

    void updatePostPublishDate(int postId, LocalDate publishDate, LocalDate updateDate, LocalTime updateTime) throws SQLException;

    List<Post> getSearchedPosts(String postTitlePart) throws SQLException;

    List<Post> getSearchedPosts(String postTitlePart, int limit, int offset) throws SQLException;
}
