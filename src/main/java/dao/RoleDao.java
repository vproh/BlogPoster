package dao;

import dao.interfaces.IDaoRole;
import dataBase.DataBaseConnection;
import models.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class RoleDao implements IDaoRole {
    /* role table column names */
    private static final String ROLE_ID_COLUMN_NAME = "role_id";
    private static final String ROLE_NAME_COLUMN_NAME = "name";

    /* SQL requests */
    private static final String GET_ROLE_BY_ID_REQUEST = "SELECT * FROM roles WHERE role_id=?";
    private static final String ADD_ROLE_REQUEST = "INSERT INTO roles("+
            ROLE_NAME_COLUMN_NAME + ") VALUES(?)";
    private static final String DROP_ROLE_BY_ID_REQUEST = "DELETE FROM roles WHERE role_id=?";
    private static final String UPDATE_ROLE_BY_ID_REQUEST = "UPDATE roles SET " +
            ROLE_NAME_COLUMN_NAME + "=? " + "WHERE role_id=?";
    private static final String GET_ALL_ROLES_REQUEST = "SELECT * from roles";


    @Override
    public Role getRoleById(int id) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_ROLE_BY_ID_REQUEST);

        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        Role role = null;

        if(resultSet.next()) {
            role = new Role();
            role.setRoleId(id);
            role.setName(resultSet.getString(ROLE_NAME_COLUMN_NAME));
        }
        resultSet.close();
        preparedStatement.close();
        connection.close();

        return role;
    }

    @Override
    public void addRole(Role role) throws SQLException {
        AddRow.addOneElementRow(ADD_ROLE_REQUEST, role.getName());
    }

    @Override
    public void deleteRoleById(int id) throws SQLException {
        DeleteRow.deleteRowByIdAndRequest(id, DROP_ROLE_BY_ID_REQUEST);
    }

    @Override
    public void updateRoleById(Role role) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROLE_BY_ID_REQUEST);

        preparedStatement.setString(1, role.getName());
        preparedStatement.setInt(2, role.getRoleId());
        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }

    @Override
    public Set<Role> getAllRole() throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_ROLES_REQUEST);
        ResultSet resultSet = preparedStatement.executeQuery();
        Set<Role> tags = new HashSet<>();

        while(resultSet.next()) {
            tags.add(getRoleById(resultSet.getInt(ROLE_ID_COLUMN_NAME)));
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        return tags;
    }
}
