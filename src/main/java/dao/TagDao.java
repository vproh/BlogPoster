package dao;

import dao.interfaces.IDaoTag;
import dataBase.DataBaseConnection;
import models.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TagDao implements IDaoTag {
    /* tag table column names */
    private static final String TAG_ID_COLUMN_NAME = "tag_id";
    private static final String TAG_NAME_COLUMN_NAME = "tag_name";

    /* SQL requests */
    private static final String GET_TAG_BY_ID_REQUEST = "SELECT * FROM tags WHERE tag_id=?";
    private static final String ADD_TAG_REQUEST = "INSERT INTO tags(" + TAG_NAME_COLUMN_NAME + ") VALUES(?)";
    private static final String DROP_TAG_BY_ID_REQUEST = "DELETE FROM tags WHERE tag_id=?";
    private static final String UPDATE_TAG_BY_ID_REQUEST = "UPDATE tags SET " +
            TAG_NAME_COLUMN_NAME + "=? " + "WHERE tag_id=?";
    private static final String GET_ALL_TAGS_REQUEST = "SELECT * from tags";
    private static final String GET_TAG_BY_NAME = "SELECT * FROM tags WHERE " + TAG_NAME_COLUMN_NAME + "=?";
    private static final String GET_TAG_NAMES = "SELECT tag_name FROM tags";

    @Override
    public Tag getTagById(int id) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_TAG_BY_ID_REQUEST);

        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        Tag tag = getTag(resultSet);

        resultSet.close();
        preparedStatement.close();
        connection.close();

        return tag;
    }

    @Override
    public void addTag(String tagName) throws SQLException {
        AddRow.addOneElementRow(ADD_TAG_REQUEST, tagName);
    }

    @Override
    public void deleteTagById(int id) throws SQLException {
        DeleteRow.deleteRowByIdAndRequest(id, DROP_TAG_BY_ID_REQUEST);
    }

    @Override
    public void updateTagById(Tag tag) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TAG_BY_ID_REQUEST);

        preparedStatement.setString(1, tag.getName());
        preparedStatement.setInt(2, tag.getTagId());
        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }

    @Override
    public Set<Tag> getAllTag() throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_TAGS_REQUEST);
        ResultSet resultSet = preparedStatement.executeQuery();
        Set<Tag> tags = new HashSet<>();

        while(resultSet.next()) {
            tags.add(getTagById(resultSet.getInt(TAG_ID_COLUMN_NAME)));
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if (tags.isEmpty())
            return null;

        return tags;
    }

    @Override
    public Tag getTagByName(String name) throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_TAG_BY_NAME);
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();

        Tag tag = getTag(resultSet);

        resultSet.close();
        preparedStatement.close();
        connection.close();

        return tag;
    }

    @Override
    public List<String> getTagNames() throws SQLException {
        Connection connection = new DataBaseConnection().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(GET_TAG_NAMES);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<String> tagNames = new ArrayList<>();

        while (resultSet.next()) {
            tagNames.add(resultSet.getString(TAG_NAME_COLUMN_NAME));
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        if (tagNames.isEmpty())
            return null;

        return tagNames;
    }

    private Tag getTag(ResultSet resultSet) throws SQLException {
        Tag tag = null;

        if(resultSet.next()) {
            tag = new Tag();
            tag.setTagId(resultSet.getInt(TAG_ID_COLUMN_NAME));
            tag.setName(resultSet.getString(TAG_NAME_COLUMN_NAME));
        }

        return tag;
    }
}
