package services;

import dao.TagDao;
import models.Tag;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TagService {
    private static final String TAG_SPLITTER = ",";

    public Set<Tag> getTagSet() throws SQLException {
        return new TagDao().getAllTag();
    }

    public Set<Tag> getTagSetByNames(String names) throws SQLException {
        Set<Tag> tags = new HashSet<>();
        Tag tag;

        if (names == null)
            return null;

        for (String tagName : names.split(TAG_SPLITTER)) {
            tag = new TagDao().getTagByName(tagName);
            if (tag != null) {
                tags.add(tag);
            }
        }

        if (tags.isEmpty())
            return null;
        return tags;
    }

    public void deleteTag(int tagId) throws SQLException {
        new TagDao().deleteTagById(tagId);
    }

    public void addTag(String tags) throws SQLException {
        TagDao tagDao = new TagDao();
        String[] tagArray = tags.split(TAG_SPLITTER);
        List<String> tagNames = tagDao.getTagNames();

        for (String tag : tagArray) {
            if (!tagNames.contains(tag))
                tagDao.addTag(tag);
        }
    }
}
