package services;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.Properties;

public class ReadFromProperty {
    private static final String UPLOADS_PROPERTY_FILE_PATH = ReadFromProperty.class.getClassLoader().getResource("uploads.properties").getPath();
    private static final String DB_PROPERTY_FILE_PATH = ReadFromProperty.class.getClassLoader().getResource("dataBase.properties").getPath();
    private static final String PROJECT_UPLOADS_DIR_KEY = "projectUploadsDirPath";
    private static final String DATA_BASE_URL_KEY = "dataBaseUrl";
    private static final String DATA_BASE_PASS_KEY = "dataBasePassword";
    private static final String DATA_BASE_USER_KEY = "dataBaseUser";
    private static final String READ_DIR_KEY = "readUploadsPath";
    private static final String UPLOADS_DIR_NAME = "uploadsDirName";
    private static final Logger LOG = Logger.getLogger(ReadFromProperty.class);

    private static Properties properties = new Properties();

    private ReadFromProperty() { throw new IllegalStateException(this.getClass().getSimpleName()); }

    public static String getProjectUploadsDirPath() {
        try(InputStream inputStream = new FileInputStream(UPLOADS_PROPERTY_FILE_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return properties.getProperty(PROJECT_UPLOADS_DIR_KEY);
    }

    public static String getUploadsDirName() {
        try(InputStream inputStream = new FileInputStream(UPLOADS_PROPERTY_FILE_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return properties.getProperty(UPLOADS_DIR_NAME);
    }

    static String getReadImageDirPath() {

        try(InputStream inputStream = new FileInputStream(UPLOADS_PROPERTY_FILE_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return properties.getProperty(READ_DIR_KEY);
    }

    public static String getUrl() {
        try(InputStream inputStream = new FileInputStream(DB_PROPERTY_FILE_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return properties.getProperty(DATA_BASE_URL_KEY);
    }

    public static String getUser() {
        try(InputStream inputStream = new FileInputStream(DB_PROPERTY_FILE_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return properties.getProperty(DATA_BASE_USER_KEY);
    }

    public static String getPassword() {
        try(InputStream inputStream = new FileInputStream(DB_PROPERTY_FILE_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return properties.getProperty(DATA_BASE_PASS_KEY);
    }
}
