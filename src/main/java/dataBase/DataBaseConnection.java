package dataBase;

import org.apache.log4j.Logger;
import services.ReadFromProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {
    private static final String URL = ReadFromProperty.getUrl();
    private static final String USER = ReadFromProperty.getUser();
    private static final String PASSWORD = ReadFromProperty.getPassword();
    private static final String POSTGRES_DRIVER_CLASS = "org.postgresql.Driver";
    private static final Logger LOG = Logger.getLogger(DataBaseConnection.class);

    private static Connection connection;

    public DataBaseConnection() {
        try {
            Class.forName(POSTGRES_DRIVER_CLASS);
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            LOG.error(e.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
