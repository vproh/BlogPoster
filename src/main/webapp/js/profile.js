$(document).ready(function() {
    var navItems = $('.user-menu li > a');
    var navListItems = $('.user-menu li');
    var allWells = $('.user-content');
    var allWellsExceptFirst = $('.user-content:not(:first)');
    allWellsExceptFirst.hide();
    console.log("doc is ready!");

    navItems.click(function(e) {
        e.preventDefault();
        navListItems.removeClass('active');
        $(this).closest('li').addClass('active');
        allWells.hide();
        var target = $(this).attr('data-target-id');
        $('#' + target).show();
    });
});
$('#deleteBtn').click(function () {
    var currentHref = $(this).attr('href');
    console.log("current href = " + currentHref);

    $('#yesModalBtn').attr('href', currentHref);
});

$('#userGeneralForm, #userDeleteAccForm, #userPasswordForm').on('keyup keypress', function(e) {
    console.log('enter pressed');
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});