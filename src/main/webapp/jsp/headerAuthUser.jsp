<header>
    <nav class="navbar navbar-default navbar-expand-lg navbar-light">
        <div class="navbar-header d-flex col">
            <a class="navbar-brand" href="explore">Blog<b>Poster</b></a>
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle navbar-toggler ml-auto">
                <span class="navbar-toggler-icon"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbarCollapse" class="w-100 collapse navbar-collapse justify-content-start">
            <div class="text-center w-100">
                <form action="search" method="get" class="w-50 navbar-form form-inline">
                    <div class="input-group search-box">
                        <input type="search" name="postTitle" id="search" class="form-control" placeholder="Search some posts...">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    </div>
                </form>
            </div>

            <ul class="nav navbar-nav ml-auto">
                <li style="width: 10rem;" class="nav-item dropdown">
                    <a style="position: relative; float: right;" data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">${currUser.getName()} ${currUser.getLastName()}<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="create" class="dropdown-item">Create new post</a></li>
                        <li><a href="profile" class="dropdown-item">My profile</a></li>
                        <li><a href="myPosts?postType=published" class="dropdown-item">My posts</a></li>
                        <li><a href="myPosts?postType=drafts" class="dropdown-item">My drafts</a></li>
                        <li><a href="myPosts?postType=collaboration" class="dropdown-item">Collaborations</a></li>
                        <li><a href="logout" class="dropdown-item">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
