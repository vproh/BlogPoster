<header>
    <nav class="navbar navbar-default navbar-expand-lg navbar-light">
        <div class="navbar-header d-flex col">
            <a class="navbar-brand" href="explore">Blog<b>Poster</b></a>
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle navbar-toggler ml-auto">
                <span class="navbar-toggler-icon"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
            <ul class="nav navbar-nav navbar-right ml-auto">
                <li class="nav-item">
                    <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">Login</a>
                    <ul class="dropdown-menu form-wrapper">
                        <li>
                            <form action="login" method="post">
                                <div class="form-group">
                                    <input name="userEmail" type="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input name="userPassword" type="password" class="form-control" placeholder="Password" required>
                                </div>
                                <input type="submit" class="btn btn-primary btn-block" value="Login"> <br>
                                <div class="w-100 text-center">
                                    <span class="errorMessage">${loginError}</span>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle get-started-btn mt-1 mb-1">Sign up</a>
                    <ul class="dropdown-menu form-wrapper">
                        <li>
                            <form action="register" method="post">
                                <p class="hint-text">Fill in this form to create your account!</p>
                                <div class="form-group">
                                    <input name="username" type="text" class="form-control" placeholder="Name" required>
                                </div>
                                <div class="form-group">
                                    <input name="lastName" type="text" class="form-control" placeholder="Last name" required>
                                </div>
                                <div class="form-group">
                                    <input name="userEmail" type="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input name="userPassword" type="password" pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
                                           title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                           class="form-control" placeholder="Password" required>
                                </div>
                                <div class="form-group">
                                    <input name="confirmUserPassword" type="password" class="form-control" placeholder="Confirm Password" required>
                                </div>
                                <div class="w-100 text-center">
                                    <c:forEach items="${registerError}" var="error">
                                        <span class="errorMessage">${error}</span>
                                    </c:forEach>
                                </div>
                                <input type="submit" class="btn btn-primary btn-block" value="Sign up">
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>