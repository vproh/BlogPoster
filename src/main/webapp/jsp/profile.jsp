<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile | BlogPoster</title>
    <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="${pageContext.request.contextPath}/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/headerStylesheet.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/profile.js"></script>
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .user-menu a{
            color: black;
        }
        .panel-heading {
            color: black !important;
            background-color: rgba(0, 250, 189, 0.51) !important;
        }
        .nav-pills > .nav-item {
            width: 100%;
        }
        .pfofileSettingsButton:hover {
            cursor: pointer;
            background-color: rgba(107,119,121,0.3);
        }
        .pfofileSettingsButton {
            color: #1c7430;
        }
    </style>
</head>
<body>
    <%@include file="headerAuthUser.jsp"%>
    <%@include file="actionModal.jsp"%>
    <div class="mt-5 container">
        <div class="row">
            <div class="mt-3 col-md-3">
                <ul class="nav nav-pills nav-stacked user-menu" >
                    <li class="pfofileSettingsButton nav-item active"><a class="nav-link" data-target-id="profile"><i class="fa fa-user"></i> Profile</a></li>
                    <li class="pfofileSettingsButton nav-item"><a class="nav-link" data-target-id="change-password"><i class="fa fa-lock"></i> Change Password</a></li>
                    <li class="pfofileSettingsButton nav-item"><a class="nav-link" data-target-id="other-settings"><i class="fa fa-cog"></i> Other</a></li>
                </ul>
            </div>

            <div class="col-md-9  user-content" id="profile">
                <form action="changeUser?change=general" method="post" id="userGeneralForm">
                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><label for="user_name" class="control-label panel-title">Name</label></h3>
                        </div>
                        <div class="panel-body">
                            <input name="userName" id="user_name" type="text" class="form-control" required value="${currUser.getName()}">
                        </div>
                    </div>
                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><label for="user_last_name" class="control-label panel-title">Last name</label></h3>
                        </div>
                        <div class="panel-body">
                            <input name="userLastName" id="user_last_name" type="text" class="form-control" required value="${currUser.getLastName()}">
                        </div>
                    </div>
                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><label for="user_email" class="control-label panel-title">Email</label></h3>
                        </div>
                        <div class="panel-body">
                            <input name="userEmail" id="user_email" type="email" class="form-control" required value="${currUser.getEmail()}">
                        </div>
                    </div>
                    <br>
                    <input class="btn btn-success btn-lg pull-right" type="submit" form="userGeneralForm" value="Save">
                </form>
            </div>

            <div class="col-md-9  user-content" id="change-password">
                <form action="changeUser?change=security" method="post" id="userPasswordForm">
                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><label for="new_password" class="control-label panel-title">New Password</label></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div>
                                    <input type="password" required pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
                                           title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                           class="form-control" name="userNewPass" id="new_password" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><label for="confirm_new_password" class="control-label panel-title">Confirm password</label></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div>
                                    <input type="password" required class="form-control" name="confirmNewPass" id="confirm_new_password" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><label for="currPass" class="control-label panel-title">Current password</label></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div>
                                    <input type="password" required class="form-control" name="currentUserPassword" id="currPass" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <input class="btn btn-success btn-lg pull-right" form="userPasswordForm" type="submit" value="Save">
                </form>
            </div>


            <div class="col-md-9  user-content" id="other-settings">
                <div class="text-center">
                    <h2>You can <b style="color: red;">delete</b> your account</h2>
                </div>
                <form action="changeUser?change=deleteAcc" method="post" id="userDeleteAccForm">

                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h4 class="panel-title"><label for="current_password" class="control-label panel-title">Current password</label></h4>
                        </div>
                        <div class="panel-body">
                            <input type="password" required class="form-control" name="currentPass" id="current_password" >
                        </div>
                    </div>
                    <div class="panel panel-info" style="margin: 1em;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><label for="confirm_current_password" class="control-label panel-title">Confirm password</label></h3>
                        </div>
                        <div class="panel-body">
                            <input type="password" required class="form-control" name="currentPassConfirm" id="confirm_current_password" >
                        </div>
                    </div>
                    <input id="deleteBtn" class="btn btn-danger btn-lg pull-right" form="userDeleteAccForm" type="submit" value="Delete" data-toggle="modal" role="button" data-target="#actionModal">
                </form>
                <c:forEach items="${updateUserError}" var="error">
                    <span class="errorMessage">${error}</span>
                </c:forEach>
            </div>
        </div>
    </div>
    <c:if test="${infoMessage != null}">
        <%@include file="infoModal.jsp"%>
    </c:if>
</body>
</html>
