<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit post | BlogPoster</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="${pageContext.request.contextPath}/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/createPost.css">
    <script src="${pageContext.request.contextPath}/jquery/jQuery3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap-tagsinput.js"></script>
</head>
    <body>
        <div class="container py-3 w-75">
            <form role="form" action="editPost?postType=${postType}&postId=${post.getPostId()}" method="post" enctype="multipart/form-data" id="editPost">
                <div class="form-group">
                    <label for="titleInput" class="control-label">Post title</label>
                    <input id="titleInput" name="postTitle" maxlength="100" type="text" required="required" class="form-control" value="${post.getTitle()}"/>
                </div><hr>
                <div class="form-group">
                    <label for="descriptionText" class="control-label">Short description</label>
                    <textarea style="height: 5rem;" id="descriptionText" name="postDescription" form="editPost" maxlength="500" type="text" required="required" class="form-control">${post.getDescription()}</textarea>
                </div><hr>
                <div class="form-group">
                    <label for="contentText" class="control-label">Post content</label>
                    <textarea style="height: 10rem;" id="contentText" name="postContent" form="editPost" maxlength="1000" type="text" required="required" class="form-control">${post.getContent()}</textarea>
                </div><hr>
                <div class="form-group">
                    <label class="control-label">Current post image:</label><br>
                    <img class="card-img-top" src="${post.getImageUrl()}" alt="Something went wrong"><br>
                    <label class="control-label">Choose new one:</label><br>
                    <input class="form-control-file" type="file" name="postImage" id="image">
                </div><br><hr>

                <div class="form-group text-center">
                    <label class="control-label">Choose some tags for post</label><br>
                    <div style="background-color: #f3f3f3;">
                        <c:forEach items="${postTags}" var="tag" >
                            <span id="${tag.getTagId()}" class="badge badge-info tagName">${tag.getName()}</span>
                        </c:forEach>
                        <br>
                    </div>
                    <input type="text" required="required" data-role="tagsinput" name="postTags" id="tags" value=""/>
                </div>
                <br>

                <c:if test="${postType != 'collaboration'}">
                        <div class="form-group">
                            <label class="control-label">Collaborators (up to 5). Delete e-mail for stop collaborate with user, add otherwise.</label><br>
                            <c:forEach items="${post.getUsers()}" var="user">
                                <input name="collaborators" maxlength="50" type="email" class="form-control" placeholder="User email..." value="${user.getEmail()}"/> <br>
                            </c:forEach>

                            <c:forEach items="${availableCollaborators}" var="i">
                                <input name="collaborators" maxlength="50" type="email" class="form-control" placeholder="User email..."  /><br>
                            </c:forEach>
                    </div>
                </c:if>
                <br>
                <input class="btn btn-success btn-lg pull-left" type="submit" value="Save">
                <a href="myPosts?postType=${postType}" class="btn btn-danger">Cancel</a>
                <div class="container w-75">
                    <c:forEach items="${editPostError}" var="error">
                        <span class="errorMessage">${error}</span>
                    </c:forEach>
                </div>
            </form>
        </div>
    <script src="${pageContext.request.contextPath}/js/tagInput.js"></script>
    </body>
</html>
