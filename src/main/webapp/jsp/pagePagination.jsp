<ul class="pagination pagination-lg">
    <c:if test="${numOfPages > 1}">
        <c:if test="${currentPage != 1}">
            <li class="page-item">
                <a class="page-link" href="${paginationUrl}currentPage=${currentPage-1}">Previous</a>
            </li>
        </c:if>
        <c:forEach begin="1" end="${numOfPages}" var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <li class="page-item active"><a class="page-link">
                            ${i} <span class="sr-only">(current)</span></a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a class="page-link" href="${paginationUrl}currentPage=${i}">${i}</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${currentPage lt numOfPages}">
            <li class="page-item">
                <a class="page-link" href="${paginationUrl}currentPage=${currentPage+1}">Next</a>
            </li>
        </c:if>
    </c:if>
</ul>