<header>
    <nav class="navbar navbar-default navbar-expand-lg navbar-light">
        <div class="navbar-header d-flex col">
            <span class="navbar-brand">Blog<b>Poster</b> | Admin</span>
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle navbar-toggler ml-auto">
                <span class="navbar-toggler-icon"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse text-center justify-content-start">
            <ul class="nav navbar-nav ml-auto">
                <li style="width: 10rem;" class="nav-item dropdown">
                    <a style="position: relative; float: right;" data-toggle="dropdown" class="nav-link dropdown-toggle">${currUser.getName()} ${currUser.getLastName()}<b class="caret"></b></a>
                    <ul class="dropdown-menu text-center">
                        <li><a href="users" class="dropdown-item">Users</a></li>
                        <li><a href="posts" class="dropdown-item">Posts</a></li>
                        <li><a href="tags" class="dropdown-item">Tags</a></li>
                        <li><a href="logout" class="dropdown-item">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>