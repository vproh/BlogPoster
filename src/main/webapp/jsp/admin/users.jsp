<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Users | BlogPoster</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/headerStylesheet.css">
    <script src="../../jquery/jQuery3.1.1.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <style>
        .btn {
            width: 5rem;
        }
    </style>
</head>
<body>
<%@include file="headerAdmin.jsp"%>
<div class="container text-center">
    <div class="row">
        <div class="col-md-12"><br>
            <h4 style="margin-bottom: 1rem;">BlogPoster Users</h4>
            <div class="table-responsive">
                <table data-sort-name="id" class="table table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Last name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Ban/Unban</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${users}" var="user">
                        <c:choose>
                            <c:when test="${user.isBlocked()}">
                                <tr class="table-danger">
                                    <td>${user.getUserId()}</td>
                                    <td><a href="posts?userId=${user.getUserId()}">${user.getName()}</a></td>
                                    <td>${user.getLastName()}</td>
                                    <td>${user.getEmail()}</td>
                                    <td>${user.getRole().getName()}</td>
                                    <td><a href="banUser?ban=false&userId=${user.getUserId()}" class="btn btn-success" role="button">Unban</a></td>
                                    
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td>${user.getUserId()}</td>
                                    <td><a href="posts?userId=${user.getUserId()}">${user.getName()}</a></td>
                                    <td>${user.getLastName()}</td>
                                    <td>${user.getEmail()}</td>
                                    <td>${user.getRole().getName()}</td>
                                    <td><a href="banUser?ban=true&userId=${user.getUserId()}" class="btn btn-danger" role="button">Ban</a></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
