<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tags | BlogPoster</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="../../bootstrap/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="../../css/headerStylesheet.css" rel="stylesheet">
    <script src="../../jquery/jQuery3.1.1.min.js"></script>
    <script src="../../bootstrap/js/bootstrap-tagsinput.js"></script>
    <style>
        .btn {
            width: 7rem !important;
        }
    </style>
</head>
    <body>
    <%@include file="headerAdmin.jsp"%>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-bottom: 1rem;">BlogPoster tags list</h4>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${tags}" var="tag">
                                <tr>
                                    <td>${tag.getTagId()}</td>
                                    <td>${tag.getName()}</td>
                                    <td><a role="button" href="deleteTag?tagId=${tag.getTagId()}" class="btn btn-danger">Delete</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>
            <br>
            <form role="form" action="addTag" method="get" id="addTagForm">
                <div class="form-group">
                    <label for="addTagInput" class="control-label">Add new tags:</label>
                    <input data-role="tagsinput" id="addTagInput" name="tagNames" maxlength="100" type="text" class="form-control" placeholder="Type to add new..."/>
                    <input class="btn btn-success" type="submit" value="Add">
                </div>
            </form>
        </div>
    </div>
    <script>
        $('#addTagForm').on('keyup keypress', function(e) {
            console.log('enter pressed');
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>
    </body>
</html>
