<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Explore</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
        <link href="${pageContext.request.contextPath}/css/blog-home.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/headerStylesheet.css">
        <script src="${pageContext.request.contextPath}/jquery/jQuery3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
        <style>
            body {
                padding: 0;
                margin: 0;
            }
        </style>
    </head>
    <body>
    <%@include file="headerAuthUser.jsp"%>
    <div class="col-md-8 mx-auto">
        <%@include file="allPostList.jsp"%>
        <nav aria-label="Search result pages">
            <%@include file="pagePagination.jsp"%>
        </nav>
    </div>
    <script type="text/javascript">
        // Prevent dropdown menu from closing when click inside the form
        $(document).on("click", ".navbar-right .dropdown-menu", function(e){
            e.stopPropagation();
        });
    </script>
    </body>
</html>
