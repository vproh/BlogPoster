<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create post | BlogPoster</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/createPost.css">
    <script src="${pageContext.request.contextPath}/jquery/jQuery3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap-tagsinput.js"></script>
    <style>
        .btn {
            width: 6rem;
        }
        .stepwizard {
            position: relative;
            width: 100%;
        }
        body {
            padding: 0;
            margin: 0;
        }
    </style>
</head>
<body>
<%@include file="actionModal.jsp"%>'
<div class="container py-3 w-75">
    <div class="stepwizard text-center">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button" class="btn btn-primary btn-circle" disabled>1</a>
                <p>Step 1</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled>2</a>
                <p>Step 2</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disable>3</a>
                <p>Step 3</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled>4</a>
                <p>Step 4</p>
            </div>
        </div>
    </div>
    <form role="form" action="create" method="post" enctype="multipart/form-data" id="addPost">
        <div class="setup-content" id="step-1">
            <h3>Step 1</h3>
            <div class="form-group">
                <label class="control-label">Post title</label>
                <input name="postTitle" maxlength="100" type="text" required class="form-control" placeholder="Yours title..." value="${postTitle}" />
            </div>
            <div class="form-group">
                <label class="control-label">Short description</label>
                <textarea style="height: 5rem" name="postDescription" form="addPost" maxlength="500" type="text" required class="form-control" placeholder="Start typing here...">${postDescription}</textarea>
            </div>
            <div class="w-100 container">
                <button class="btn btn-primary nextBtn btn-lg pull-left" type="button" >Next</button>
                <a href="explore" class="btn btn-danger pull-left">Cancel</a>
            </div>
        </div>
        <div class="setup-content" id="step-2">
            <h3>Step 2</h3>
            <div class="form-group">
                <label class="control-label">Post content</label>
                <textarea style="height: 10rem;" name="postContent" form="addPost" maxlength="1000" type="text" required class="form-control" placeholder="Start typing here...">${postContent}</textarea>
            </div>
            <div class="form-group">
                <c:choose>
                    <c:when test="${postImage != null}">
                        <h3>Downloaded image:</h3><br>
                        <img src="${postImage}">
                    </c:when>
                    <c:otherwise>
                        <label class="control-label">Choose a picture for your Post</label>
                        <input required class="form-control-file" type="file" name="postImage" id="image">
                    </c:otherwise>
                </c:choose>
            </div><br>
            <button class="btn btn-primary nextBtn btn-lg pull-left" type="button" >Next</button>
        </div>
        <div class="setup-content" id="step-3">
            <h3>Step 3</h3>
            <div class="form-group text-center">
                <label class="control-label">Choose some tags for post</label><br>
                <div style="background-color: #f3f3f3;">
                    <c:forEach items="${postTags}" var="tag" >
                        <span id="${tag.getTagId()}" class="badge badge-info tagName">${tag.getName()}</span>
                    </c:forEach>
                    <br>
                </div>
                <input type="text" required data-role="tagsinput" name="postTags" id="tags" value=""/>
            </div>
            <br>
            <div class="form-group">
                <label class="control-label">and invite collaborators (up to 5)</label><br>
                <c:choose>
                    <c:when test="${errors == null}">
                        <input name="collaborators" maxlength="50" type="email" class="form-control" placeholder="User email..."  /><br>
                        <input name="collaborators" maxlength="50" type="email" class="form-control" placeholder="User email..."  /><br>
                        <input name="collaborators" maxlength="50" type="email" class="form-control" placeholder="User email..."  /><br>
                        <input name="collaborators" maxlength="50" type="email" class="form-control" placeholder="User email..."  /><br>
                        <input name="collaborators" maxlength="50" type="email" class="form-control" placeholder="User email..."  /><br>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${collaborators}" var="user">
                            <input name="collab" maxlength="50" type="email" class="form-control" value="${user.getEmail()}" /><br>
                        </c:forEach>
                        <c:forEach items="${availableCollaborators}" var="i">
                            <input name="collab" maxlength="50" type="email" class="form-control" placeholder="User email..."  /><br>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </div>
            <button class="btn btn-pfrimary nextBtn btn-lg pull-left" type="button" >Next</button>
        </div>
        <div class="container mx-auto">
            <c:forEach items="${errors}" var="error">
                <span class="errorMessage">${error}</span>
            </c:forEach>
        </div>
        <div class="setup-content" id="step-4">
            <h3>Step 4</h3>
            <input onclick="form.action='/create?isPublish=true'" class="btn btn-success btn-lg pull-left" type="submit" value="Finish!"><div class="or-seperator"><b>or</b></div>
            <input onclick="form.action='/create?isPublish=false'" class="btn btn-lg pull-left" type="submit" value="Save as draft">
        </div>
    </form>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/createPost.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/tagInput.js"></script>
</body>
</html>
