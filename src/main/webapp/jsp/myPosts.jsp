<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My posts | BlogPoster</title>
    <link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="${pageContext.request.contextPath}/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/postTab.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/headerStylesheet.css">
    <script src="${pageContext.request.contextPath}/jquery/jQuery3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<%@include file="headerAuthUser.jsp"%>
<%@include file="../html/confirmActionModal.html"%>
    <div class="container-fluid w-75">
        <section id="tabs">
                <div class="col-xs-12 ">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link ${tab1Active}" role="tab" href="myPosts?postType=published">Published(${publishedAmount})</a>
                            <a class="nav-item nav-link ${tab2Active}" role="tab" href="myPosts?postType=drafts">Drafts(${draftsAmount})</a>
                            <a class="nav-item nav-link ${tab3Active}" role="tab" href="myPosts?postType=collaboration">Collaborations(${collabAmount})</a>
                        </div>
                    </nav>
                    <div class="py-3 px-3 px-sm-0">
                            <c:choose>
                                <c:when test="${posts != null}">
                                <div class="tab-pane">
                                    <c:forEach items="${posts}" var="post">
                                        <h1 class="my-4">${post.getTitle()}</h1>
                                        <p class="lead"><i class="fa fa-user" aria-hidden="true"></i> by
                                            <c:forEach items="${post.getUsers()}" var="user"> <a href="search?searchType=user&id=${user.getUserId()}">${user.getName()} ${user.getLastName()}</a> </c:forEach>
                                        </p>
                                        <hr>
                                        <c:if test="${post.getDateOfPublishing() != null}">
                                            <p><i class="fa fa-calendar"></i> Posted on ${post.getDateOfPublishing()}
                                                <c:if test="${post.getDateOfUpdate() != null && post.getDateOfUpdate() != post.getDateOfPublishing()}"> <i>(Updated: ${post.getDateOfUpdate()} on ${post.getTimeOfUpdate()}) </i> </c:if>
                                            </p>
                                        </c:if>
                                        <p><i class="fa fa-tags"></i> Tags:
                                            <c:forEach items="${post.getTagList()}" var="tag"> <a href="search?searchType=tag&id=${tag.getTagId()}"><span class="badge badge-info">${tag.getName()}</span></a>
                                            </c:forEach>
                                        </p>
                                        <hr>
                                        <div class="card mb-4">
                                            <img class="card-img-top" src="${post.getImageUrl()}" alt="Something went wrong">
                                            <div class="card-body">
                                                <p class="card-text">${post.getDescription()}</p>
                                                <a href="post?postId=${post.getPostId()}" class="btn btn-primary">Read More</a>
                                                <c:if test="${tab2Active == 'active'}">
                                                    <a href="publish?postType=${postType}&postId=${post.getPostId()}" class="btn btn-success pull-right"data-toggle="modal" data-target="#actionModal">Publish</a>
                                                </c:if>
                                                <a href="editPost?postType=${postType}&postId=${post.getPostId()}" class="btn btn-warning pull-right" data-toggle="modal" data-target="#actionModal">Edit</a>
                                                <c:if test="${tab3Active != 'active'}">
                                                    <a href="deletePost?postType=${postType}&postId=${post.getPostId()}" class="btn btn-danger pull-right" data-toggle="modal" data-target="#actionModal">Delete</a>
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <nav aria-label="drafts search pages">
                                        <%@include file="pagePagination.jsp"%>
                                    </nav>
                                </div>
                                </c:when>
                                <c:otherwise>
                                <div class="container w-100 tab-pane text-center">
                                    <div class="error-template mx-auto">
                                        <h3 style="color: darkgrey">
                                            Hmmm...
                                        </h3>
                                        <h4 style="color: darkgrey">
                                            Looks like there is no posts yet
                                        </h4>
                                        <div class="error-actions">
                                            <br>
                                            <a href="create" class="w-25 btn btn-primary"><span class="glyphicon glyphicon-home"></span>
                                                Create post </a>
                                        </div>
                                    </div>
                                </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
        </section>
                </div>
    <c:if test="${infoMessage != null}">
        <%@include file="infoModal.jsp"%>
    </c:if>
<script>
    $('.btn').click(function () {
        var currentHref = $(this).attr('href');
        console.log("current href = " + currentHref);

        $('#yesModalBtn').attr('href', currentHref);
    });
</script>
</body>
</html>
